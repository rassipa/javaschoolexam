package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class PyramidBuilder {


    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

        int rows = getNumberOfRows(inputNumbers.size());
        int cols = rows * 2 - 1;
        int start=0,end=cols;   //for working with loop in matrix's rows

        for (int i = 0; i < inputNumbers.size(); i++) {
            if (inputNumbers.get(i) == null)
                throw new CannotBuildPyramidException();
        }
        Collections.sort(inputNumbers);

        Stack<Integer> stack = new Stack<>();
        for (int i = 0; i < inputNumbers.size(); i++)
            stack.push(inputNumbers.get(i));


        int[][] matrix = new int[rows][cols];       //Fill in the matrix, starting from the lower right position, because of using stack
        for (int i = 0; i < rows; i++)
            for (int j = 0; j < rows; j++)
                matrix[i][j] = 0;

        for (int i = rows - 1; i >= 0 ; i --){
            for (int j = end - 1; j >= start ; j-=2) {
                matrix[i][j] = stack.pop();

            }
            start++;
            end--;
        }
        return matrix;
    }

    //Calculate the number or rows int the matrix
    public static int getNumberOfRows(int n) throws CannotBuildPyramidException {
        if (n == 0)
            throw new CannotBuildPyramidException();

        if (n == 1)
            return 1;
        double temp = (Math.sqrt(1+8*n) - 1)/2;  //formula of triangle numbers
        int counter=0;
        if (temp % 1 == 0) {
            for (int i = 0; i < n; i+=counter) {
                counter++;
            }
        } else
            throw new CannotBuildPyramidException();

        return counter;
    }


}
