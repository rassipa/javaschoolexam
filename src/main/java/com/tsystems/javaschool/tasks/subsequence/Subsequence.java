package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;
import java.util.Stack;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) throws IllegalArgumentException {
        if (x == null || y == null)
            throw new IllegalArgumentException();

        if (x.size() == 0)
            return true;

        Stack xStack = new Stack();
        Stack yStack = new Stack();
        Object xElement, yElement;


        for (Object temp : x)
            xStack.push(temp);

        for (Object temp : y)
            yStack.push(temp);

        xElement = xStack.pop();
        //compare elements from x and y stack while yStack is not empty, if during this time xStack has become empty, it means
        // that X - subsequence of Y
        while (!yStack.empty()) {
            yElement = yStack.pop();

            if (xElement.equals(yElement)) {
                if (xStack.empty()) {
                    return true;
                }

                xElement = xStack.pop();
            }
        }

        return false;
    }
}
