package com.tsystems.javaschool.tasks.calculator;

import java.util.ArrayDeque;
import java.util.Deque;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        try {
            String rpn = generationRpn(statement);
            return calculate(rpn);

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        return null;
    }


    // Generation reverse polish notation (RPN)
    private static String generationRpn(String statement) throws Exception {
        StringBuilder stack = new StringBuilder(""),    // stack for operators
                out   = new StringBuilder("");    // output string in RPN

        char symbol, temp;  //symbol - current character from input string, temp - character from stack

        for (int i = 0; i < statement.length(); i++) {      //go through all elements from input string
            symbol = statement.charAt(i);
            if (isOperator(symbol)) {

                //put a character from the input string in priority on the stack (the lowest priority is in the tail)
                while (stack.length() > 0) {
                    temp = stack.substring(stack.length()-1).charAt(0);
                    if (isOperator(temp) && (getPriority(symbol) <= getPriority(temp))) {
                        out.append(" ").append(temp).append(" ");
                        stack.setLength(stack.length()-1);
                    } else {
                        out.append(" ");
                        break;
                    }
                }
                out.append(" ");
                stack.append(symbol);
            } else if (symbol == '(') {
                stack.append(symbol);

                // if the input character is a closing bracket, then all operators in the stack up to the opening bracket
                // put in the output string
            } else if (symbol == ')') {
                temp = stack.substring(stack.length()-1).charAt(0);
                while (temp != '(') {
                    if (stack.length() < 1) {
                        return null;
                    }
                    out.append(" ").append(temp);
                    stack.setLength(stack.length()-1);
                    temp = stack.substring(stack.length()-1).charAt(0);
                }
                stack.setLength(stack.length()-1);
            } else {
                out.append(symbol);   // If character - digit, directly put in the output string
            }
        }

        // After parsing the input string, add the remaining operators in the stack to the output string
        while (stack.length() > 0) {
            out.append(" ").append(stack.substring(stack.length()-1));
            stack.setLength(stack.length()-1);
        }

        return  out.toString();
    }

    private static boolean isOperator(char c) {
        switch (c) {
            case '-':
            case '+':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private static byte getPriority(char operator) {
        switch (operator) {
            case '*':
            case '/':
                return 2;
        }
        return 1; // + или -
    }

    //Calculate statement in RPN
    private static String calculate(String rpn) throws Exception {
        double firstNumber = 0, secondNumber = 0;
        Deque<Double> stack = new ArrayDeque<Double>();

        rpn = rpn.replaceAll("\\s{2,}", " ");
        String[] tokens = rpn.split(" ");



        //if temp(the input character of the RPN string) is a digit , then we put it on the stack, if it is an operator, then we
        // perform arithmetic operation with the first two numbers
        for (String temp : tokens) {
            try {
                if (temp.length() == 1 && isOperator(temp.charAt(0))) {
                    if (stack.size() < 2) {
                        return null;
                    }
                    secondNumber = stack.pop();
                    firstNumber = stack.pop();
                    switch (temp.charAt(0)) {
                        case '+':
                            firstNumber += secondNumber;
                            break;
                        case '-':
                            firstNumber -= secondNumber;
                            break;
                        case '/':
                            firstNumber /= secondNumber;
                            break;
                        case '*':
                            firstNumber *= secondNumber;
                            break;

                        default:
                            return null;
                    }
                    stack.push(firstNumber);
                } else {
                    firstNumber = Double.parseDouble(temp);
                    stack.push(firstNumber);
                }
            } catch (Exception e) {
                return null;
            }
        }

        if (stack.size() > 1) {
            return null;
        }

        String result = stack.pop().toString();
        result = result.replaceAll("\\.0$","");

        if (result.equals("Infinity"))
            result = null;

        return result;
    }

}




